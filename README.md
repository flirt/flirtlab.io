
* Online documentation of the FLIRT rendering framework.
* The code of the framework is opensource and can be found over [there](https://gitlab.com/flirt/FLIRT).
